CREATE TABLE developer (
  id BIGSERIAL,
  uid      VARCHAR(50) NOT NULL,
  password VARCHAR(70) NOT NULL,
  email    VARCHAR(255) NOT NULL UNIQUE,
  enabled  BOOLEAN,
  PRIMARY KEY (id)
);

CREATE TABLE script (
  id BIGSERIAL,
  uid           VARCHAR(50) NOT NULL,
  description   VARCHAR(300),
  code          VARCHAR(1500),
  developer_id BIGINT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (developer_id) REFERENCES developer (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE source (
  id BIGSERIAL,
  uid           VARCHAR(50) NOT NULL,
  developer_id BIGINT NOT NULL,
  type          VARCHAR(100) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (developer_id) REFERENCES developer (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE verification (
  id BIGSERIAL,
  uid             VARCHAR(50) NOT NULL,
  developer_id   BIGINT NOT NULL,
  expiration_date TIMESTAMP WITH TIME ZONE,
  PRIMARY KEY (id),
  FOREIGN KEY (developer_id) REFERENCES developer (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE recovery (
  id BIGSERIAL,
  uid             VARCHAR(50) NOT NULL,
  developer_id   BIGINT NOT NULL,
  expiration_date TIMESTAMP WITH TIME ZONE,
  PRIMARY KEY (id),
  FOREIGN KEY (developer_id) REFERENCES developer (id) ON DELETE CASCADE ON UPDATE CASCADE
);


CREATE TABLE script_source (
  source_id           BIGINT NOT NULL,
  script_id           BIGINT NOT NULL,
  PRIMARY KEY (source_id, script_id),
  CONSTRAINT dev_source_fk FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT dev_script_fk FOREIGN KEY (script_id) REFERENCES script (id) ON DELETE CASCADE ON UPDATE CASCADE
);

CREATE TABLE attribute (
  id BIGSERIAL,
  uid           VARCHAR(50) NOT NULL,
  source_id    BIGINT NOT NULL,
  value         VARCHAR(1500) NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (source_id) REFERENCES source (id) ON DELETE CASCADE ON UPDATE CASCADE
);