package com.jnk.kb.controllers.rest.interfaces.base;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

/**
 * Created by Raman_Susla1
 * kb
 * 11/28/2017
 */
public interface IController {
	default <T> ResponseEntity<T> ok(T result) {
		return new ResponseEntity<>(result, HttpStatus.OK);
	}
	default ResponseEntity forbid(){return new ResponseEntity(HttpStatus.FORBIDDEN);}
	default ResponseEntity notFound(){return new ResponseEntity(HttpStatus.NOT_FOUND);}
	default ResponseEntity okEmpty(){return new ResponseEntity(HttpStatus.NO_CONTENT);}
}
