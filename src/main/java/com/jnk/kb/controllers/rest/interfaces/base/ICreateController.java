package com.jnk.kb.controllers.rest.interfaces.base;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.services.interfaces.base.ICreateService;
import com.jnk.kb.views.View;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

public interface ICreateController<Type> extends IController {
	ICreateService<Type, Long> getCreteService();

	@RequestMapping(method = RequestMethod.POST)
	@ResponseBody
	@JsonView(View.Advanced.class)
	default ResponseEntity post(@PathVariable Map<String, String> pathVariables, @Valid @RequestBody Type object, Principal principal) {
		return ok(getCreteService().create(object));
	}
}
