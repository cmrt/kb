package com.jnk.kb.controllers.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.controllers.rest.interfaces.base.IController;
import com.jnk.kb.services.interfaces.IVerificationService;
import com.jnk.kb.views.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@RestController
@RequestMapping("/developer/{developerUid}")
@JsonView(View.Simple.class)
public class VerificationController implements IController {
	@Autowired private IVerificationService service;

	@RequestMapping(value = "/verify/{token}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity verify(@PathVariable("developerUid") String developerUid, @PathVariable("token") String token) {
		return service.verify(developerUid, token) == null ? notFound() : okEmpty();
	}

	@RequestMapping(value = "/reset/{token}", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity reset(@PathVariable("developerUid") String developerUid, @PathVariable("token") String token) {
		return service.reset(developerUid, token) == null ? notFound() : okEmpty();
	}
}
