package com.jnk.kb.controllers.rest.interfaces.base;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.services.interfaces.base.IUpdateService;
import com.jnk.kb.views.View;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.Map;

public interface IUpdateController<Type> extends IController {
	IUpdateService<Type, Long> getUpdateService();

	@RequestMapping(value = "{id}", method = RequestMethod.PUT)
	@ResponseBody
	@JsonView(View.Advanced.class)
	default ResponseEntity put(@PathVariable Map<String, String> pathVariables, @Valid @RequestBody Type object, Principal principal) {
		object = getUpdateService().update(object);
		return object == null ? notFound() : ok(object);
	}
}
