package com.jnk.kb.controllers.rest.interfaces.base;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.services.interfaces.base.IGetOneByUidService;
import com.jnk.kb.views.View;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.Map;

public interface IGetOneByUidController<Type> extends IController {
	IGetOneByUidService<Type, Long> getGetByIdService();

	@RequestMapping(value = "{id}", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(View.Advanced.class)
	default ResponseEntity get(@PathVariable Map<String, String> pathVariables, Principal principal) {
		Type object = getGetByIdService().get(Long.parseLong(pathVariables.get("id")));
		return object == null ? notFound() : ok(object);
	}
}
