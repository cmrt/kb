package com.jnk.kb.controllers.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.controllers.rest.interfaces.base.ICrudController;
import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.services.interfaces.IDeveloperService;
import com.jnk.kb.services.interfaces.IScriptService;
import com.jnk.kb.services.interfaces.ISourceService;
import com.jnk.kb.services.interfaces.base.ICrudService;
import com.jnk.kb.views.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Map;


/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@RestController
@RequestMapping("/developer")
public class DeveloperController implements ICrudController<DeveloperEntity> {
	@Autowired private IDeveloperService developerService;
	@Autowired private ISourceService sourceService;
	@Autowired private IScriptService scriptService;

	@Override
	public ICrudService<DeveloperEntity, Long> getCrudRepository() {
		return developerService;
	}

	@RequestMapping(value = "{id}/sources", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(View.Simple.class)
	public ResponseEntity<Page> getSources(@PathVariable("id") Long id, Pageable pageable) {
		final Page<SourceEntity> sources = sourceService.getAllByDeveloper(id, pageable);
		return ok(sources);
	}

	@RequestMapping(value = "{id}/scripts", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(View.Simple.class)
	public ResponseEntity<Page> getScripts(@PathVariable("id") Long id, Pageable pageable) {
		final Page<ScriptEntity> scripts = scriptService.getAllByDeveloper(id, pageable);
		return ok(scripts);
	}

	@Override
	@JsonView({View.Advanced.class, View.Developer.class})
	public ResponseEntity get(@PathVariable Map<String, String> pathVariables, Principal principal) {
		return ICrudController.super.get(pathVariables, principal);
	}
}
