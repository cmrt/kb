package com.jnk.kb.controllers.rest.interfaces.base;

import com.jnk.kb.services.interfaces.base.IGetAllPagedSortedService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.Map;

public interface IGetPagedSortedController<Type> extends IController {
	IGetAllPagedSortedService<Type, Long> getPagedSortedService();

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	default ResponseEntity get(@PathVariable Map<String, String> pathVariables, Pageable pageable, Principal principal) {
		Page<Type> page = getPagedSortedService().getAll(pageable);
		return page == null ? notFound() : ok(page);
	}
}
