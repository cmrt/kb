package com.jnk.kb.controllers.rest.interfaces.base;


import com.jnk.kb.services.interfaces.base.*;


/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface ICrudController<Type>
	extends ICreateController<Type>, IDeleteByUidController<Type>, IUpdateController<Type>, IGetOneByUidController<Type> {
	ICrudService<Type, Long> getCrudRepository();

	@Override
	default IGetOneByUidService<Type, Long> getGetByIdService() {
		return getCrudRepository();
	}

	@Override default IDeleteByUidService<Type, Long> getDeleteService() {
		return getCrudRepository();
	}

	@Override default IUpdateService<Type, Long> getUpdateService() {
		return getCrudRepository();
	}

	@Override default ICreateService<Type, Long> getCreteService() {
		return getCrudRepository();
	}
}
