package com.jnk.kb.controllers.rest;

import com.jnk.kb.exceptions.DeveloperAlreadyExistsException;
import com.jnk.kb.exceptions.EmailAlreadyRegisteredException;
import com.jnk.kb.exceptions.SourceAttributeException;
import com.jnk.kb.exceptions.VerificationExpiredException;
import com.jnk.kb.helpers.ServletHelper;
import com.jnk.kb.models.Action;
import com.jnk.kb.models.ErrorResult;
import org.hibernate.exception.ConstraintViolationException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;

import javax.script.ScriptException;
import java.util.Collections;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
@ControllerAdvice
public class ErrorController {
	@Value("${confirm.url-reset}") private String resetUrl;
	@Autowired private ServletHelper servletHelper;

	@ExceptionHandler(IllegalArgumentException.class)
	public ResponseEntity<ErrorResult> IllegalArgumentExceptionHandler(IllegalArgumentException e) {
		return new ResponseEntity<>(new ErrorResult(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(ScriptException.class)
	public ResponseEntity<ErrorResult> ScriptExceptionHandler(ScriptException e) {
		return new ResponseEntity<>(new ErrorResult(e.getMessage()), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	@ExceptionHandler(VerificationExpiredException.class)
	public ResponseEntity<ErrorResult> VerificationExpiredExceptionHandler(VerificationExpiredException e) {
		final Action action = new Action(HttpMethod.GET.name(),
			servletHelper.getBaseUrl() + String.format(resetUrl, e.getDeveloperId(), e.getToken()));
		return new ResponseEntity<>(new ErrorResult("verification token expired", Collections.singletonList(action)), HttpStatus.UNAUTHORIZED);
	}

	@ExceptionHandler(DeveloperAlreadyExistsException.class)
	public ResponseEntity<ErrorResult> DeveloperAlreadyExistsExceptionHandler(DeveloperAlreadyExistsException e) {
		return new ResponseEntity<>(new ErrorResult("developer already exists"), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(EmailAlreadyRegisteredException.class)
	public ResponseEntity<ErrorResult> EmailAlreadyRegisteredExceptionHandler(EmailAlreadyRegisteredException e) {
		return new ResponseEntity<>(new ErrorResult("email already registered"), HttpStatus.CONFLICT);
	}

	@ExceptionHandler(EmptyResultDataAccessException.class)
	public ResponseEntity<ErrorResult> EmptyResultDataAccessExceptionHandler(EmptyResultDataAccessException e) {
		return new ResponseEntity<>(new ErrorResult("Not found"), HttpStatus.NOT_FOUND);
	}


	@ExceptionHandler(SourceAttributeException.class)
	public ResponseEntity<ErrorResult> SourceAttributeExceptionHandler(SourceAttributeException e) {
		return new ResponseEntity<>(new ErrorResult(e.getAttribute() + "not found"), HttpStatus.INTERNAL_SERVER_ERROR);
	}
}
