package com.jnk.kb.controllers.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.controllers.rest.interfaces.base.IController;
import com.jnk.kb.services.interfaces.ISourceExecutorResolverService;
import com.jnk.kb.services.sources.ISourceExtractorService;
import com.jnk.kb.views.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.stream.Collectors;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@RestController
@RequestMapping("/executors")
@JsonView(View.Simple.class)
public class SourceExecutorController implements IController {
	@Autowired private ISourceExecutorResolverService service;

	@RequestMapping(method = RequestMethod.GET)
	@ResponseBody
	@JsonView(View.Simple.class)
	public ResponseEntity getAll(Principal principal) {
		return ok(service.getExtractors().stream().map(ISourceExtractorService::getSourceExecutorName).collect(Collectors.toList()));
	}

	@RequestMapping(value = "{name}/attributes",method = RequestMethod.GET)
	@ResponseBody
	@JsonView(View.Simple.class)
	public ResponseEntity getAttributesByName(@PathVariable("name")String name, Principal principal) {
		return ok(service.getExtractorAttributes(name));
	}
}
