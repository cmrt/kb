package com.jnk.kb.controllers.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.controllers.rest.interfaces.base.IController;
import com.jnk.kb.models.Password;
import com.jnk.kb.services.interfaces.IRecoveryService;
import com.jnk.kb.views.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/developer/{developerUid}")
@JsonView(View.Simple.class)
public class RecoveryController implements IController {
	@Autowired private IRecoveryService service;

	@RequestMapping(value = "/recover", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity askRecovery(@PathVariable("developerId") String developerUid) {
		return service.askRecovery(developerUid) == null ? notFound() : okEmpty();
	}

	@RequestMapping(value = "/recover/{token}", method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity askRecovery(@PathVariable("developerId") String developerUid, @PathVariable("token") String token, @RequestBody Password password) {
		return service.recover(developerUid, token, password.getPassword()) == null ? notFound() : okEmpty();
	}
}
