package com.jnk.kb.controllers.rest.interfaces.base;


/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface ICrudPagedSortedController<Type> extends ICrudController<Type>, IGetPagedSortedController<Type> {
}
