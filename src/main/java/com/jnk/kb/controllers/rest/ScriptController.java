package com.jnk.kb.controllers.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.controllers.rest.interfaces.base.ICrudPagedSortedController;
import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.models.EvaluationResult;
import com.jnk.kb.services.interfaces.IEvaluatorService;
import com.jnk.kb.services.interfaces.IPurifierService;
import com.jnk.kb.services.interfaces.IScriptService;
import com.jnk.kb.services.interfaces.base.ICrudService;
import com.jnk.kb.services.interfaces.base.IGetAllPagedSortedService;
import com.jnk.kb.views.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.script.ScriptException;


/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@RestController
@RequestMapping("/script")
@JsonView(View.Simple.class)
public class ScriptController implements ICrudPagedSortedController<ScriptEntity> {
	@Autowired private IScriptService scriptService;
	@Autowired private IEvaluatorService evaluatorService;
	@Autowired private IPurifierService purifierService;


	@RequestMapping(value = "{id}/code", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getSource(@PathVariable("id") Long id) {
		final ScriptEntity script = scriptService.get(id);
		return ok(script.getCode());
	}

	@RequestMapping(value = "{id}/purified", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<String> getPurified(@PathVariable("id") Long id) {
		final ScriptEntity script = scriptService.get(id);
		return ok(purifierService.purify(script.getCode()));
	}

	@RequestMapping(value = "{id}/evaluated", method = RequestMethod.GET)
	@ResponseBody
	public ResponseEntity<EvaluationResult> getEvaluated(@PathVariable("id") Long id) throws ScriptException {
		final ScriptEntity script = scriptService.get(id);
		return ok(evaluatorService.evaluate(script));
	}

	@Override
	public ICrudService<ScriptEntity, Long> getCrudRepository() {
		return scriptService;
	}

	@Override
	public IGetAllPagedSortedService<ScriptEntity, Long> getPagedSortedService() {
		return scriptService;
	}
}
