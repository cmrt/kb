package com.jnk.kb.controllers.rest.interfaces.base;

import com.jnk.kb.services.interfaces.base.IDeleteByUidService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.Map;

public interface IDeleteByUidController<Type> extends IController {
	IDeleteByUidService<Type, Long> getDeleteService();

	@RequestMapping(value = "{id}", method = RequestMethod.DELETE)
	@ResponseBody
	default ResponseEntity delete(@PathVariable Map<String, String> pathVariables, Principal principal) {
		getDeleteService().delete(Long.parseLong(pathVariables.get("id")));
		return new ResponseEntity<>(HttpStatus.NO_CONTENT);
	}
}
