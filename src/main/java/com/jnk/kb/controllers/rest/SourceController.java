package com.jnk.kb.controllers.rest;

import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.controllers.rest.interfaces.base.ICrudPagedSortedController;
import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.services.interfaces.ISourceExecutorResolverService;
import com.jnk.kb.services.interfaces.ISourceService;
import com.jnk.kb.services.interfaces.base.ICrudService;
import com.jnk.kb.services.interfaces.base.IGetAllPagedSortedService;
import com.jnk.kb.views.View;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@RestController
@RequestMapping("/source")
@JsonView(View.Simple.class)
public class SourceController implements ICrudPagedSortedController<SourceEntity> {
	@Autowired private ISourceService service;
	@Autowired private ISourceExecutorResolverService extractorResolverService;

	@RequestMapping(value = "{id}/request", method = RequestMethod.GET)
	@ResponseBody
	@JsonView(View.Simple.class)
	public ResponseEntity requestSource(@PathVariable("id")Long id, Principal principal) {
		SourceEntity source = getGetByIdService().get(id);
		return source == null? notFound(): ok(extractorResolverService.executeSource(source));
	}

	@Override
	public ICrudService<SourceEntity, Long> getCrudRepository() {
		return service;
	}

	@Override
	public IGetAllPagedSortedService<SourceEntity, Long> getPagedSortedService() {
		return service;
	}
}
