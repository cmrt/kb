package com.jnk.kb.helpers;

import lombok.Data;
import org.springframework.stereotype.Component;
import org.springframework.web.context.request.RequestAttributes;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * Created by catmo_000 on 12/3/2017.
 */
@Component
@Data
public class ServletHelper {
	public String getBaseUrl(HttpServletRequest request) {
		return request.getRequestURL().substring(0, request.getRequestURL().length() - request.getRequestURI().length()) + request.getContextPath();
	}

	public String getBaseUrl() {
		final RequestAttributes requestAttributes = RequestContextHolder.getRequestAttributes();
		final HttpServletRequest request = ((ServletRequestAttributes) requestAttributes).getRequest();
		return getBaseUrl(request);
	}
}
