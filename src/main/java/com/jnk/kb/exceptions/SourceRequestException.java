package com.jnk.kb.exceptions;

/**
 * Created by catmo_000 on 12/8/2017.
 */
public class SourceRequestException extends RuntimeException {
	public SourceRequestException(Throwable cause) {
		super(cause);
	}
}
