package com.jnk.kb.exceptions;

import com.jnk.kb.models.IAttributeType;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by catmo_000 on 12/8/2017.
 */
@Data
@AllArgsConstructor
public class SourceAttributeException extends RuntimeException {
	private IAttributeType attribute;
}
