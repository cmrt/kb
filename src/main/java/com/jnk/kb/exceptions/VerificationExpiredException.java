package com.jnk.kb.exceptions;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by catmo_000 on 12/1/2017.
 */
@Data
@AllArgsConstructor
public class VerificationExpiredException extends RuntimeException {
	private Long developerId;
	private String token;
}
