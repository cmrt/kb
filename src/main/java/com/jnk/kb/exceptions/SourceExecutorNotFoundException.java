package com.jnk.kb.exceptions;

import com.jnk.kb.entities.SourceEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by catmo_000 on 12/9/2017.
 */
@Data
@AllArgsConstructor
public class SourceExecutorNotFoundException extends RuntimeException {
	private String entity;
}
