package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.exceptions.SourceExecutorNotFoundException;
import com.jnk.kb.models.IAttributeType;
import com.jnk.kb.services.sources.ISourceExtractorService;

import java.util.Set;

/**
 * Created by catmo_000 on 12/9/2017.
 */
public interface ISourceExecutorResolverService {
	ISourceExtractorService getExtractorByName(String name);
	Set<ISourceExtractorService> getExtractors();
	Set<IAttributeType> getExtractorAttributes(String name);
	Object executeSource(SourceEntity entity);
	default void validateSourceExecutorName(SourceEntity entity){
		if(getExtractorByName(entity.getType()) == null) throw new SourceExecutorNotFoundException(entity.getType());
	}
}
