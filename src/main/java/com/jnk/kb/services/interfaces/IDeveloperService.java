package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.services.interfaces.base.ICrudService;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
public interface IDeveloperService extends ICrudService<DeveloperEntity, Long> {
	DeveloperEntity changePassword(Long id, String password);
	DeveloperEntity getByUid(String uid);
}
