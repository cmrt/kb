package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface ICrudService<Type, Id extends Serializable> extends ICudService<Type, Id>, IGetOneByUidService<Type, Id> {}

