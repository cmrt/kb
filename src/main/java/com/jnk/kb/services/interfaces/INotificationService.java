package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.RecoveryEntity;
import com.jnk.kb.entities.VerificationEntity;

import javax.mail.MessagingException;
import java.io.IOException;

/**
 * Created by catmo_000 on 12/3/2017.
 */
public interface INotificationService {
	void sendConfirmation(DeveloperEntity developer, VerificationEntity verification);
	void sendRecovery(DeveloperEntity developer, RecoveryEntity recoveryEntity);
}
