package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.models.EvaluationResult;

import javax.script.ScriptException;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
public interface IEvaluatorService {
	EvaluationResult evaluate(ScriptEntity script) throws ScriptException;
}
