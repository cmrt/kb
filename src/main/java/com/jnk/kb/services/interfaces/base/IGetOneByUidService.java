package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

public interface IGetOneByUidService<Type, Id extends Serializable> {
  Type get(Id id);
}
