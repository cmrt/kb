package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

public interface ICreateService<Type, Id extends Serializable> {
	Type create(Type object);
}
