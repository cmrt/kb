package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface ICrudPagedSortedService<Type, Id extends Serializable> extends ICrudService<Type, Id>, IGetAllPagedSortedService<Type, Id> {
}

