package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface IUpdateService<Type, Id extends Serializable> {
	Type update(Type object);
}

