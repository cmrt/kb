package com.jnk.kb.services.interfaces.extentions;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Raman_Susla1
 * kb
 * 12/4/2017
 */
public interface IDateExtension {
	default Date minutesFromNow(int toAdd) {
		return fromNow(Calendar.MINUTE, toAdd);
	}
	default Date fromNow(int type, int toAdd) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Timestamp(cal.getTime().getTime()));
		cal.add(type, toAdd);
		return new Date(cal.getTime().getTime());
	}

	default boolean isFuture(Date date)
	{
		final Calendar cal = Calendar.getInstance();
		return date.getTime() - cal.getTime().getTime() <= 0;
	}
}
