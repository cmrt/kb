package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.VerificationEntity;
import com.jnk.kb.services.interfaces.base.ICrudService;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
public interface IVerificationService extends ICrudService<VerificationEntity, Long> {
	VerificationEntity verify(String developerId, String token);
	VerificationEntity reset(String developerId, String token);
}
