package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.services.interfaces.base.ICrudPagedSortedService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
public interface IScriptService extends ICrudPagedSortedService<ScriptEntity, Long> {
	Page<ScriptEntity> getAllByDeveloper(Long id, Pageable pageable);
}
