package com.jnk.kb.services.interfaces;

import com.jnk.kb.entities.RecoveryEntity;
import com.jnk.kb.services.interfaces.base.ICrudService;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
public interface IRecoveryService extends ICrudService<RecoveryEntity, Long> {
	RecoveryEntity askRecovery(String uid);
	RecoveryEntity recover(String uid, String token, String password);
}
