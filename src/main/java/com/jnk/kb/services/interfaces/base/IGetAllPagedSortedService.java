package com.jnk.kb.services.interfaces.base;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

/**
 * Created by Raman_Susla1
 * kb
 * 1/9/2018
 */
public interface IGetAllPagedSortedService<Type, Id> {
	Page<Type> getAll(Pageable pageable);
}
