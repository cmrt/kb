package com.jnk.kb.services.interfaces;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
public interface IPurifierService {
	String purify(String code);
}
