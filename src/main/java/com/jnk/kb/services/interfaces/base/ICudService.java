package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface ICudService<Type, Id extends Serializable> extends IUpdateService<Type, Id>, ICreateService<Type, Id>, IDeleteByUidService<Type, Id> {}
