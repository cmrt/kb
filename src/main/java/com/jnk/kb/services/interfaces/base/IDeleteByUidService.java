package com.jnk.kb.services.interfaces.base;

import java.io.Serializable;

public interface IDeleteByUidService<Type, Id extends Serializable> {
	void delete(Id id);
}
