package com.jnk.kb.services;

import com.jnk.kb.services.interfaces.base.ICrudService;
import org.springframework.data.repository.CrudRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public abstract class AbstractService<Entity, Key extends Serializable> implements ICrudService<Entity, Key> {

	protected abstract CrudRepository<Entity, Key> getRepository();

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Entity create(Entity object) {
		return getRepository().save(object);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public void delete(Key key) {
		getRepository().delete(key);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Entity get(Key key) {
		return getRepository().findOne(key);
	}

	@Override
	@Transactional(propagation = Propagation.SUPPORTS)
	public Entity update(Entity object) {
		return getRepository().save(object);
	}

}

