package com.jnk.kb.services;

import com.jnk.kb.services.interfaces.base.IGetAllPagedSortedService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public abstract class AbstractPagedSortedService<Entity, Key extends Serializable> extends AbstractService<Entity, Key> implements IGetAllPagedSortedService<Entity, Key> {

	protected abstract PagingAndSortingRepository<Entity, Key> getPagingSortingRepository();

	@Transactional(propagation = Propagation.SUPPORTS)
	@Override
	public Page<Entity> getAll(Pageable pageable){
		return getPagingSortingRepository().findAll(pageable);
	}
}

