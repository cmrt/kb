package com.jnk.kb.services;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.RecoveryEntity;
import com.jnk.kb.entities.VerificationEntity;
import com.jnk.kb.helpers.ServletHelper;
import com.jnk.kb.models.mails.ConfirmationMail;
import com.jnk.kb.models.mails.RecoveryMail;
import com.jnk.kb.services.interfaces.INotificationService;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.ClassPathResource;
import org.springframework.mail.MailSendException;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.ui.freemarker.FreeMarkerTemplateUtils;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

@Service
public class NotificationService implements INotificationService {
	@Autowired private JavaMailSender sender;
	@Autowired private ServletHelper servletHelper;

	@Value("${confirm.url}") private String registrationConfirmUrl;
	@Value("${confirm.message-subject}") private String registrationMessageSubject;
	@Value("${recovery.message-subject}") private String recoveryMessageSubject;

	@Qualifier("getFreeMarkerConfiguration") @Autowired private Configuration freemarkerConfig;


	@Override public void sendConfirmation(DeveloperEntity developer, VerificationEntity verification) {
		final ConfirmationMail mail = new ConfirmationMail(developer, verification);
		try {
			send(developer.getEmail(), registrationMessageSubject, "registration-confirmation.ftl", mail);
		} catch (TemplateException | IOException | MessagingException e) {
			throw new MailSendException("confirmation e-mail delivery failure", e);
		}
	}

	@Override public void sendRecovery(DeveloperEntity developer, RecoveryEntity recovery) {
		final RecoveryMail mail = new RecoveryMail(developer, recovery);
		try {
			send(developer.getEmail(), recoveryMessageSubject, "password-recovery.ftl", mail);
		} catch (MessagingException | IOException | TemplateException e) {
			throw new MailSendException("Recovery e-mail delivery failure", e);
		}
	}

	private void send(String sendTo, String subject, String template, Object mail) throws MessagingException, IOException, TemplateException {
		MimeMessage message = sender.createMimeMessage();
		MimeMessageHelper helper = new MimeMessageHelper(message, MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED, StandardCharsets.UTF_8.name());
		Template t = freemarkerConfig.getTemplate(template);
		String html = FreeMarkerTemplateUtils.processTemplateIntoString(t, mail);
		helper.setTo(sendTo);
		helper.setSubject(subject);
		helper.setText(html, true);
		sender.send(message);
	}
}
