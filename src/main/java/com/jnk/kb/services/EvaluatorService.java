package com.jnk.kb.services;

import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.models.EvaluationResult;
import com.jnk.kb.services.interfaces.IEvaluatorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.script.ScriptEngine;
import javax.script.ScriptException;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
@Service
public class EvaluatorService implements IEvaluatorService {
	@Autowired private ScriptEngine engine;

	@Override
	public EvaluationResult evaluate(ScriptEntity script) throws ScriptException {
		Object result = engine.eval(script.getCode());
		return new EvaluationResult(result);
	}
}
