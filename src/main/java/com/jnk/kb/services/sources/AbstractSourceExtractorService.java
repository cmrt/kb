package com.jnk.kb.services.sources;

import com.jnk.kb.entities.AttributeEntity;
import com.jnk.kb.exceptions.SourceAttributeException;
import com.jnk.kb.models.IAttributeType;

import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by catmo_000 on 12/8/2017.
 */
public abstract class AbstractSourceExtractorService implements ISourceExtractorService {
	protected final Map<IAttributeType, Optional<AttributeEntity>> getAttributesMap(List<AttributeEntity> attributes) {
		final Set<IAttributeType> attributesTypes = getSourceAttributes();
		final Map<IAttributeType, Optional<AttributeEntity>> resultMap = new ConcurrentHashMap<>();
		for (IAttributeType attribute : attributesTypes) {
			Optional<AttributeEntity> result = attributes.stream().filter(entity -> entity.getUid().equals(attribute.getName())).findFirst();
			if (!result.isPresent() && attribute.isMandatory()) throw new SourceAttributeException(attribute);
			resultMap.put(attribute, result);
		}
		return resultMap;
	}
}
