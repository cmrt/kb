package com.jnk.kb.services.sources;

import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.models.IAttributeType;

import java.util.Set;

/**
 * Created by catmo_000 on 12/8/2017.
 */
public interface ISourceExtractorService {
	String getSourceExecutorName();
	Set<IAttributeType> getSourceAttributes();
	Object request(SourceEntity source);

}
