package com.jnk.kb.services.sources;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.jnk.kb.entities.AttributeEntity;
import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.exceptions.SourceRequestException;
import com.jnk.kb.models.IAttributeType;

import com.jnk.kb.views.View;
import lombok.AllArgsConstructor;
import lombok.Data;
import okhttp3.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.util.*;

/**
 * Created by catmo_000 on 12/8/2017.
 */
@Service
public class HttpSourceExtractor extends AbstractSourceExtractorService implements ISourceExtractorService {
	@Autowired private ObjectMapper mapper;
	@Autowired private OkHttpClient client;
	private final static TypeReference<Map<String, String>> TYPE_REFERENCE = new TypeReference<Map<String, String>>() {
	};

	private static final HttpAttribute HEADERS = new HttpAttribute("HEADERS", false);
	private static final HttpAttribute BODY = new HttpAttribute("BODY", false);
	private static final HttpAttribute METHOD = new HttpAttribute("METHOD", false);
	private static final HttpAttribute URL = new HttpAttribute("URL", true);

	private final static Set<IAttributeType> sourceAttributes = new HashSet<>(Arrays.asList(HEADERS, BODY, METHOD, URL));

	@Override public String getSourceExecutorName() {
		return "HTTP";
	}

	@Override public Set<IAttributeType> getSourceAttributes() {
		return sourceAttributes;
	}

	@Override public Object request(SourceEntity source) {
		try {
			List<AttributeEntity> attributes = source.getAttributes();
			Map<IAttributeType, Optional<AttributeEntity>> attributesMap = getAttributesMap(attributes);
			Optional<AttributeEntity> headersEntity = attributesMap.get(HEADERS);
			Optional<AttributeEntity> bodyEntity = attributesMap.get(BODY);
			Optional<AttributeEntity> methodEntity = attributesMap.get(METHOD);

			Request.Builder requestBuilder = new Request.Builder();
			requestBuilder.url(attributesMap.get(URL).get().getValue());
			Map<String, Object> headersMap = new HashMap<>();
			if (headersEntity.isPresent()) headersMap = mapper.readValue(headersEntity.get().getValue(), TYPE_REFERENCE);
			headersMap.forEach((key, value) -> requestBuilder.addHeader(key, String.valueOf(value)));

			requestBuilder.method(methodEntity.map(AttributeEntity::getValue).orElse("GET"),
				bodyEntity.map(entity -> RequestBody.create(null, entity.getValue())).orElse(null));


			Response response = client.newCall(requestBuilder.build()).execute();
			final Map<String, Object> result = new HashMap<>();
			result.put("code", response.code());
			result.put("headers", response.headers().toMultimap());
			String responseBody = "";
			try {
				responseBody = response.body().string();
			} catch (NullPointerException ignored) {
			}
			result.put("response", responseBody);

			return result;
		} catch (IOException e) {
			throw new SourceRequestException(e);
		}
	}

	@Data
	@AllArgsConstructor
	private static final class HttpAttribute implements IAttributeType {
		@JsonProperty("name")
		@JsonView(View.Simple.class)
		private String name;
		@JsonProperty("mandatory")
		@JsonView(View.Simple.class)
		private boolean mandatory;
	}
}
