package com.jnk.kb.services;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.ScriptEntity;
import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.repositories.IScriptRepository;
import com.jnk.kb.services.interfaces.IDeveloperService;
import com.jnk.kb.services.interfaces.IScriptService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

@Service
public class ScriptService extends AbstractPagedSortedService<ScriptEntity, Long> implements IScriptService {
	@Autowired private IScriptRepository repository;

	@Override
	protected CrudRepository getRepository() {
		return repository;
	}

	@Override
	protected PagingAndSortingRepository<ScriptEntity, Long> getPagingSortingRepository() {
		return repository;
	}

	@Override
	public Page<ScriptEntity> getAllByDeveloper(Long id, Pageable pageable) {
		return repository.findAllByDeveloperId(id, pageable);
	}
}
