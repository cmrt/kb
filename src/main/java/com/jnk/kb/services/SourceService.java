package com.jnk.kb.services;

import com.jnk.kb.entities.DeveloperDependentEntity;
import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.exceptions.SourceExecutorNotFoundException;
import com.jnk.kb.repositories.ISourceRepository;
import com.jnk.kb.services.interfaces.IDeveloperService;
import com.jnk.kb.services.interfaces.ISourceExecutorResolverService;
import com.jnk.kb.services.interfaces.ISourceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class SourceService extends AbstractPagedSortedService<SourceEntity, Long> implements ISourceService {
	@Autowired
	private ISourceRepository repository;
	@Autowired
	private ISourceExecutorResolverService executorResolverService;
	@Autowired
	private IDeveloperService developerService;

	@Override
	protected CrudRepository getRepository() {
		return repository;
	}

	@Transactional
	@Override
	public SourceEntity create(SourceEntity source) {
		executorResolverService.validateSourceExecutorName(source);
		return super.create(source);
	}

	@Transactional
	@Override
	public SourceEntity update(SourceEntity source) {
		executorResolverService.validateSourceExecutorName(source);
		return super.update(source);
	}

	@Override
	protected PagingAndSortingRepository<SourceEntity, Long> getPagingSortingRepository() {
		return repository;
	}

	@Override
	public Page<SourceEntity> getAllByDeveloper(Long id, Pageable pageable) {
		return repository.findAllByDeveloperId(id, pageable);
	}
}
