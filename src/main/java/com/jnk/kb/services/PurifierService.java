package com.jnk.kb.services;

import com.google.common.primitives.Chars;
import com.jnk.kb.services.interfaces.IPurifierService;
import org.springframework.stereotype.Service;

import java.util.*;

import static com.jnk.kb.services.PurifierService.State.*;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
@Service
public class PurifierService implements IPurifierService {
	enum State {
		QUOTE_STRING, DOUBLE_QUOTE_STRING, CODE, CODE_AFTER_SPACE, SINGLE_LINE_COMMENT, MULTIPLE_LINE_COMMENT,
		MULTIPLE_LINE_COMMENT_START_CLOSE, COMMENT_START, TRY_SPEC, SPECIAL
	}

	private static List<char[]> SPECIALS = new ArrayList<>();

	static {
		Arrays.asList("var", "return", "else", "new", "function").forEach(str -> SPECIALS.add(str.toCharArray()));
	}

	@Override
	public String purify(String code) {
		if (code == null)
			return "";
		final char[] chars = code.toCharArray();
		final List<Character> charsList = new ArrayList<>(Chars.asList(chars));
		State state = CODE_AFTER_SPACE;
		final List<Character> specialChars = new ArrayList<>();
		for (int i = 0; i < charsList.size(); i++) {
			char c = charsList.get(i);
			switch (c) {
				case '/':
					switch (state) {
						case TRY_SPEC:
							specialChars.clear();
						case CODE:
						case SPECIAL:
						case CODE_AFTER_SPACE:
							state = COMMENT_START;
							break;
						case COMMENT_START:
							state = SINGLE_LINE_COMMENT;
							charsList.remove(i--);
							charsList.remove(i--);
							break;
						case MULTIPLE_LINE_COMMENT_START_CLOSE:
							state = CODE;
							charsList.remove(i--);
							charsList.remove(i--);
							break;
					}
					break;
				case '*':
					switch (state) {
						case COMMENT_START:
							state = MULTIPLE_LINE_COMMENT;
							break;
						case MULTIPLE_LINE_COMMENT:
							state = MULTIPLE_LINE_COMMENT_START_CLOSE;
							break;
						case TRY_SPEC:
							specialChars.clear();
							break;
					}
					break;
				case ' ':
				case '\t':
				case '\n':
					switch (state) {
						case TRY_SPEC:
							specialChars.clear();
						case CODE:
						case CODE_AFTER_SPACE:
						case SINGLE_LINE_COMMENT:
							charsList.remove(i--);
							state = CODE_AFTER_SPACE;
							break;
						case SPECIAL:
							state = CODE_AFTER_SPACE;
					}
					break;
				case '\'':
					switch (state) {
						case TRY_SPEC:
							specialChars.clear();
						case CODE:
						case CODE_AFTER_SPACE:
							state = QUOTE_STRING;
							break;
						case QUOTE_STRING:
							state = CODE;
							break;
					}
					break;
				case '\"':
					switch (state) {
						case TRY_SPEC:
							specialChars.clear();
						case CODE:
						case CODE_AFTER_SPACE:
							state = DOUBLE_QUOTE_STRING;
							break;
						case DOUBLE_QUOTE_STRING:
							state = CODE;
							break;
					}
					break;
				default:
					switch (state) {
						case SINGLE_LINE_COMMENT:
						case MULTIPLE_LINE_COMMENT:
							charsList.remove(i--);
							break;
						case CODE_AFTER_SPACE:
						case TRY_SPEC:
							specialChars.add(c);
							char[] special = startWithSpecials(Chars.toArray(specialChars), SPECIALS);
							if (special != null)
								state = specialChars.size() == special.length ? SPECIAL : TRY_SPEC;
							else {
								state = CODE;
								specialChars.clear();
							}
							break;
						case SPECIAL:
							state = CODE;
							break;
					}
					break;
			}
		}
		return String.valueOf(Chars.toArray(charsList));
	}

	private char[] startWithSpecials(char[] value, List<char[]> specials) {
		return specials.stream().filter(special -> startsWith(special, value))
			.sorted(Comparator.comparingInt(special -> -special.length)).findFirst().orElse(null);
	}

	private boolean startsWith(char[] value, char[] prefix) {
		int to = 0;
		int po = 0;
		int pc = prefix.length;
		if ((to > value.length - pc))
			return false;
		while (--pc >= 0)
			if (value[to++] != prefix[po++])
				return false;
		return true;
	}
}
