package com.jnk.kb.services;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.RecoveryEntity;
import com.jnk.kb.repositories.IRecoveryRepository;
import com.jnk.kb.services.interfaces.IDeveloperService;
import com.jnk.kb.services.interfaces.INotificationService;
import com.jnk.kb.services.interfaces.IRecoveryService;
import com.jnk.kb.services.interfaces.extentions.IDateExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class RecoveryService extends AbstractService<RecoveryEntity, Long> implements IRecoveryService, IDateExtension {
	@Autowired private IRecoveryRepository repository;
	@Autowired private IDeveloperService developerService;
	@Autowired private INotificationService notificationService;

	@Value("${recovery.token-expiration}") private int tokenExpiration;

	@Override
	protected IRecoveryRepository getRepository() {
		return repository;
	}

	@Override
	public RecoveryEntity create(RecoveryEntity recovery) {
		recovery.setUid(UUID.randomUUID().toString());
		recovery.setExpirationDate(minutesFromNow(tokenExpiration));
		return super.create(recovery);
	}

	@Override
	public RecoveryEntity update(RecoveryEntity recovery) {
		recovery.setExpirationDate(minutesFromNow(tokenExpiration));
		return super.update(recovery);
	}

	@Override
	@Transactional
	public RecoveryEntity askRecovery(String developerUid) {
		DeveloperEntity developer = developerService.getByUid(developerUid);
		if (developer == null) return null;
		RecoveryEntity recovery = new RecoveryEntity();
		recovery.setDeveloper(developer);
		recovery = create(recovery);
		notificationService.sendRecovery(developer, recovery);
		return recovery;
	}

	@Override
	@Transactional
	public RecoveryEntity recover(String developerUid, String token, String password) {
		final RecoveryEntity recovery = repository.findRecoveryEntityByUid(token);
		if (recovery == null || !recovery.getDeveloper().getUid().equals(developerUid)) return null;
		final DeveloperEntity developer = developerService.changePassword(recovery.getDeveloper().getId(), password);
		if(developer == null) return null;
		delete(recovery.getId());
		return recovery;
	}
}
