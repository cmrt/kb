package com.jnk.kb.services;

import com.jnk.kb.entities.SourceEntity;
import com.jnk.kb.exceptions.SourceExecutorNotFoundException;
import com.jnk.kb.models.IAttributeType;
import com.jnk.kb.services.interfaces.ISourceExecutorResolverService;
import com.jnk.kb.services.sources.ISourceExtractorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * Created by catmo_000 on 12/9/2017.
 */
@Service
public class SourceExecutorResolverService implements ISourceExecutorResolverService {
	@Autowired
	private Set<ISourceExtractorService> services;

	@Override public ISourceExtractorService getExtractorByName(String name) {
		return services.stream().filter(extractor->extractor.getSourceExecutorName().equals(name)).findFirst().orElse(null);
	}

	@Override public Set<ISourceExtractorService> getExtractors() {
		return services;
	}

	@Override public Set<IAttributeType> getExtractorAttributes(String name) {
		return getExtractorByName(name).getSourceAttributes();
	}

	@Override public Object executeSource(SourceEntity entity) {
		return getExtractorByName(entity.getType()).request(entity);
	}
}
