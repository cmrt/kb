package com.jnk.kb.services;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.VerificationEntity;
import com.jnk.kb.exceptions.DeveloperAlreadyExistsException;
import com.jnk.kb.exceptions.EmailAlreadyRegisteredException;
import com.jnk.kb.models.Authenticated;
import com.jnk.kb.repositories.IDeveloperRepository;
import com.jnk.kb.services.interfaces.IDeveloperService;
import com.jnk.kb.services.interfaces.INotificationService;
import com.jnk.kb.services.interfaces.IVerificationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.CrudRepository;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import static java.util.Collections.emptyList;

@Service
public class DeveloperService extends AbstractService<DeveloperEntity, Long> implements IDeveloperService,
	UserDetailsService {
	@Autowired private INotificationService notificationService;
	@Autowired private IDeveloperRepository repository;
	@Autowired private IVerificationService verificationService;
	@Autowired private PasswordEncoder passwordEncoder;

	@Override
	protected CrudRepository getRepository() {
		return repository;
	}

	@Override
	public UserDetails loadUserByUsername(String uid) throws UsernameNotFoundException {
		DeveloperEntity entity = repository.findDeveloperEntityByUid(uid);
		if (entity == null) throw new UsernameNotFoundException(uid);
		return new Authenticated(entity.getId(), entity.getUid(), entity.getPassword(), entity.isEnabled(), true, true, true, emptyList());
	}

	@Override
	@Transactional
	public DeveloperEntity create(DeveloperEntity developer) {
		if (repository.findDeveloperEntityByUid(developer.getUid()) != null) throw new DeveloperAlreadyExistsException();
		if (repository.findDeveloperEntityByEmail(developer.getEmail()) != null) throw new EmailAlreadyRegisteredException();
		developer.setPassword(passwordEncoder.encode(developer.getPassword()));
		final DeveloperEntity createdDeveloper = super.create(developer);
		if (createdDeveloper != null) {
			VerificationEntity verification = new VerificationEntity();
			verification.setDeveloper(createdDeveloper);
			verification = verificationService.create(verification);
			notificationService.sendConfirmation(developer, verification);
		}
		return createdDeveloper;
	}

	@Transactional(propagation = Propagation.SUPPORTS)
	@Override
	public DeveloperEntity changePassword(Long developerId, String password) {
		DeveloperEntity entity = get(developerId);
		if (entity != null) {
			entity.setPassword(passwordEncoder.encode(password));
		}
		return entity;
	}

	@Override
	public DeveloperEntity getByUid(String uid) {
		return repository.findDeveloperEntityByUid(uid);
	}
}
