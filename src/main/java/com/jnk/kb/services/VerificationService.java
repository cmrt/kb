package com.jnk.kb.services;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.VerificationEntity;
import com.jnk.kb.exceptions.VerificationExpiredException;
import com.jnk.kb.repositories.IVerificationRepository;
import com.jnk.kb.services.interfaces.IDeveloperService;
import com.jnk.kb.services.interfaces.INotificationService;
import com.jnk.kb.services.interfaces.IVerificationService;
import com.jnk.kb.services.interfaces.extentions.IDateExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.UUID;

@Service
public class VerificationService extends AbstractService<VerificationEntity, Long> implements IVerificationService, IDateExtension {
	@Autowired private IVerificationRepository repository;
	@Autowired private IDeveloperService developerService;
	@Autowired private INotificationService notificationService;

	@Value("${confirm.token-expiration}") private int tokenExpiration;

	@Override
	protected IVerificationRepository getRepository() {
		return repository;
	}

	@Override
	public VerificationEntity create(VerificationEntity verification) {
		verification.setUid(UUID.randomUUID().toString());
		verification.setExpirationDate(minutesFromNow(tokenExpiration));
		return super.create(verification);
	}

	@Override
	public VerificationEntity update(VerificationEntity verification) {
		verification.setExpirationDate(minutesFromNow(tokenExpiration));
		return super.update(verification);
	}

	@Override
	@Transactional
	public VerificationEntity verify(String developerId, String token) {
		final VerificationEntity verification = repository.findVerificationEntityByUid(token);
		if (verification == null || !verification.getDeveloper().getUid().equals(developerId)) return null;
		if (isFuture(verification.getExpirationDate())) throw new VerificationExpiredException(verification.getId(), token);
		final DeveloperEntity developer = verification.getDeveloper();
		developer.setEnabled(true);
		developerService.update(developer);
		delete(verification.getId());
		return verification;
	}

	@Override
	@Transactional
	public VerificationEntity reset(String developerId, String token) {
		VerificationEntity verification = repository.findVerificationEntityByUid(token);
		if (verification == null) return null;
		delete(verification.getId());
		verification = create(verification);
		notificationService.sendConfirmation(verification.getDeveloper(), verification);
		return verification;
	}

}
