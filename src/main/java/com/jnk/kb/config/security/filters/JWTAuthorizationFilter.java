package com.jnk.kb.config.security.filters;

import com.jnk.kb.App;
import com.jnk.kb.models.Authenticated;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static java.util.Collections.emptyList;

public class JWTAuthorizationFilter extends BasicAuthenticationFilter implements SecurityConstants {

	private final byte[] secret;

	public JWTAuthorizationFilter(AuthenticationManager authManager, String secret) {
		super(authManager);
		this.secret = secret.getBytes();
	}

	@Override
	protected void doFilterInternal(HttpServletRequest req, HttpServletResponse res, FilterChain chain)
		throws IOException, ServletException {
		String header = req.getHeader(HEADER_STRING);

		if (header == null || !header.startsWith(TOKEN_PREFIX)) {
			chain.doFilter(req, res);
			return;
		}

		UsernamePasswordAuthenticationToken authentication = null;
		Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(header.replace(TOKEN_PREFIX, "")).getBody();
		String developer = claims.getSubject();
		Object userId = claims.get("userId");
		if (developer != null && userId != null) {
			Authenticated auth = new Authenticated(Long.valueOf(String.valueOf(userId)), developer);
			authentication = new UsernamePasswordAuthenticationToken(auth, null, new ArrayList<>());
		}
		SecurityContextHolder.getContext().setAuthentication(authentication);
		chain.doFilter(req, res);
	}
}