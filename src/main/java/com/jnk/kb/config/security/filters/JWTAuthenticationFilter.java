package com.jnk.kb.config.security.filters;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.jnk.kb.App;
import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.models.Authenticated;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;

public class JWTAuthenticationFilter extends UsernamePasswordAuthenticationFilter implements SecurityConstants {
	private AuthenticationManager authenticationManager;

	private final byte[] secret;
	private final long expirationTime;

	public JWTAuthenticationFilter(AuthenticationManager authenticationManager, String secret, long expirationTime) {
		this.authenticationManager = authenticationManager;
		this.secret = secret.getBytes();
		this.expirationTime = expirationTime;
	}

	@Override
	public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws AuthenticationException {
		try {
			DeveloperEntity credentials = new ObjectMapper().readValue(req.getInputStream(), DeveloperEntity.class);
			return authenticationManager
				.authenticate(new UsernamePasswordAuthenticationToken(credentials.getUid(), credentials.getPassword(), new ArrayList<>()));
		}
		catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	@Override
	protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth)
		throws IOException, ServletException {
		Authenticated authenticated = (Authenticated) auth.getPrincipal();
		String token = Jwts.builder().setSubject(authenticated.getUsername())
			.claim("userId", authenticated.getId())
			.setExpiration(new Date(System.currentTimeMillis() + expirationTime)).signWith(SignatureAlgorithm.HS512, secret)
			.compact();
		res.addHeader(HEADER_STRING, TOKEN_PREFIX + token);
		res.setStatus(HttpServletResponse.SC_OK);
		res.setContentType("application/json");
		res.setCharacterEncoding("UTF-8");
		ObjectMapper mapper = new ObjectMapper();
		DeveloperEntity entity = new DeveloperEntity();
		entity.setId(authenticated.getId());
		entity.setUid(authenticated.getUsername());
		String json =  mapper.writeValueAsString(entity);
		res.getWriter().write(json);
	}
}