package com.jnk.kb.config.security;

import com.jnk.kb.config.security.filters.JWTAuthenticationFilter;
import com.jnk.kb.config.security.filters.JWTAuthorizationFilter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.util.Arrays;
import java.util.stream.Stream;

@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class WebSecurity extends WebSecurityConfigurerAdapter {
	private static final String[] REST_POST_PERMIT_ALL = {"/developer", "/developer/**/recover/**"};
	private static final String[] REST_GET_PERMIT_ALL = {"/developer/**/script/**/source",
		"/developer/**/recover",
		"/developer/**/script/**/purified",
		"/developer/**/script/**/evaluated",
		"/developer/**/verify/**",
		"/developer/**/reset/**"};


	@Value("${jwt-token.secter}")
	private String secret;
	@Value("${jwt-token.expiration}")
	private long expirationTime;

	@Autowired
	private UserDetailsService userDetailsService;
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		final AuthenticationManager authenticationManager = authenticationManager();
		http.cors().and().csrf().disable().authorizeRequests().antMatchers(HttpMethod.POST, REST_POST_PERMIT_ALL).permitAll()
			.antMatchers(HttpMethod.GET, REST_GET_PERMIT_ALL).permitAll()
			.anyRequest().authenticated().and().addFilter(new JWTAuthenticationFilter(authenticationManager, secret, expirationTime))
			.addFilter(new JWTAuthorizationFilter(authenticationManager, secret)).sessionManagement()
			.sessionCreationPolicy(SessionCreationPolicy.STATELESS);
	}

	@Override
	public void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.userDetailsService(userDetailsService).passwordEncoder(passwordEncoder);
	}

	@Override
	public void configure(org.springframework.security.config.annotation.web.builders.WebSecurity web) throws Exception {
		web.ignoring().antMatchers("/css/**");
		web.ignoring().antMatchers("/js/**");
		web.ignoring().antMatchers("/fonts/**");
	}
}