package com.jnk.kb.config.security.filters;

public interface SecurityConstants {
	String TOKEN_PREFIX = "Bearer ";
	String HEADER_STRING = "Authorization";
}