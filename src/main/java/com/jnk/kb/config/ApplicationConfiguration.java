package com.jnk.kb.config;

import com.google.common.collect.ImmutableList;
import com.jnk.kb.helpers.ServletHelper;
import okhttp3.OkHttpClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.context.annotation.Import;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import javax.script.ScriptEngine;
import javax.script.ScriptEngineManager;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.Executor;

import static org.springframework.http.HttpMethod.*;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@Configuration
@EnableAsync
@Import({FreemarkerConfiguration.class})
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class ApplicationConfiguration {
	@Value("#{'${cors.headers}'.split(',')}")
	private List<String> headers;

	@Bean
	public ScriptEngine scriptEngine() {
		ScriptEngineManager factory = new ScriptEngineManager();
		return factory.getEngineByName("nashorn");
	}

	@Bean
	public PasswordEncoder passwordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public CorsConfigurationSource corsConfigurationSource() {
		final CorsConfiguration config = new CorsConfiguration().applyPermitDefaultValues();
		config.setAllowedMethods(Arrays.asList(GET.name(), POST.name(), PUT.name(), DELETE.name(), OPTIONS.name(), HEAD.name()));
		config.setAllowedHeaders(headers);
		config.setExposedHeaders(headers);
		config.setAllowedOrigins(ImmutableList.of("*"));
		final UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", config);
		return source;
	}

	@Bean
	public Executor threadPoolTaskExecutor() {
		return new ThreadPoolTaskExecutor();
	}

	@Bean
	public OkHttpClient okHttpClient() {
		return new OkHttpClient();
	}
}
