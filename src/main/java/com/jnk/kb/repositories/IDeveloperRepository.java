package com.jnk.kb.repositories;

import com.jnk.kb.entities.DeveloperEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
public interface IDeveloperRepository extends CrudRepository<DeveloperEntity, Long> {
	DeveloperEntity findDeveloperEntityByEmail(String email);
	DeveloperEntity findDeveloperEntityByUid(String uid);
}
