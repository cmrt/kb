package com.jnk.kb.repositories;

import com.jnk.kb.entities.ScriptEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by Raman_Susla1
 * kb
 * 10/31/2017
 */
public interface IScriptRepository extends CrudRepository<ScriptEntity, Long>, PagingAndSortingRepository<ScriptEntity, Long> {
	@Query(value = "select s from ScriptEntity s where s.developer.id = :developerUid")
	Page<ScriptEntity> findAllByDeveloperId(@Param("developerUid") Long developerUid, Pageable pageable);
}
