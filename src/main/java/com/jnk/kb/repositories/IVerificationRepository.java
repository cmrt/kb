package com.jnk.kb.repositories;

import com.jnk.kb.entities.VerificationEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Raman_Susla1
 * kb
 * 11/28/2017
 */
public interface IVerificationRepository extends CrudRepository<VerificationEntity, Long> {
	VerificationEntity findVerificationEntityByUid(String token);
}
