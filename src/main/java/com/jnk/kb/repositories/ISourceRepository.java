package com.jnk.kb.repositories;

import com.jnk.kb.entities.SourceEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 * Created by Raman_Susla1
 * kb
 * 11/28/2017
 */
public interface ISourceRepository extends CrudRepository<SourceEntity, Long>, PagingAndSortingRepository<SourceEntity, Long> {
	@Query(value = "select s from SourceEntity s where s.developer.id = :developerId")
	Page<SourceEntity> findAllByDeveloperId(@Param("developerId") Long developerId, Pageable pageable);
}
