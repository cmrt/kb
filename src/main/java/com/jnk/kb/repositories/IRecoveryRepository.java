package com.jnk.kb.repositories;

import com.jnk.kb.entities.RecoveryEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Raman_Susla1
 * kb
 * 11/28/2017
 */
public interface IRecoveryRepository extends CrudRepository<RecoveryEntity, Long> {
	RecoveryEntity findRecoveryEntityByUid(String uid);
}
