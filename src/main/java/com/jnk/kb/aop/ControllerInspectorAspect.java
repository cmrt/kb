package com.jnk.kb.aop;

import com.jnk.kb.controllers.rest.interfaces.base.IController;
import com.jnk.kb.entities.DeveloperDependentEntity;
import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.models.Authenticated;
import com.jnk.kb.services.interfaces.IDeveloperService;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.stereotype.Component;

import java.util.Map;

/**
 * Created by catmo_000 on 12/2/2017.
 */
@Component
@Aspect
public class ControllerInspectorAspect implements IController {
	@Autowired private IDeveloperService developerService;
	@Pointcut("execution(* com.jnk.kb.controllers.rest.interfaces.base.ICreateController.post(..))") public void post() { }
	@Pointcut("execution(* com.jnk.kb.controllers.rest.interfaces.base.IUpdateController.put(..))") public void put() { }
	@Pointcut("execution(* com.jnk.kb.controllers.rest.interfaces.base.IDeleteByUidController.delete(..))") public void delete() { }
	@Pointcut("execution(* com.jnk.kb.controllers.rest.interfaces.base.IGetOneByUidController.get(..))") public void getOne() { }
	@Pointcut("target(com.jnk.kb.controllers.rest.DeveloperController+)") public void developer() { }
	@Pointcut("target(com.jnk.kb.controllers.rest.ScriptController+)") public void script() { }
	@Pointcut("target(com.jnk.kb.controllers.rest.SourceController+)") public void source() { }
	@Pointcut("target(com.jnk.kb.controllers.rest.VerificationController+)") public void verification() { }

	@Around("delete() && source()") public Object deleteSource(ProceedingJoinPoint joinPoint) throws Throwable { return deleteScriptOrSource(joinPoint); }
	@Around("delete() && script()") public Object deleteScript(ProceedingJoinPoint joinPoint) throws Throwable { return deleteScriptOrSource(joinPoint); }
	@Around("post() && source()") public Object postSource(ProceedingJoinPoint joinPoint) throws Throwable { return postScriptOrSource(joinPoint); }
	@Around("post() && script()") public Object postScript(ProceedingJoinPoint joinPoint) throws Throwable { return postScriptOrSource(joinPoint); }
	@Around("put() && source()") public Object putSource(ProceedingJoinPoint joinPoint) throws Throwable { return putScriptOrSource(joinPoint); }
	@Around("put() && script()") public Object putScript(ProceedingJoinPoint joinPoint) throws Throwable { return putScriptOrSource(joinPoint); }

	public Object deleteScriptOrSource(ProceedingJoinPoint joinPoint) throws Throwable {
		final Object[] arguments = joinPoint.getArgs();
		final Map<String, String> pathVariables = (Map<String, String>) arguments[0];
		final Authenticated principal = (Authenticated) ((UsernamePasswordAuthenticationToken) arguments[1]).getPrincipal();
		if (!principal.getId().equals(Long.parseLong(pathVariables.get("id")))) return forbid();
		return joinPoint.proceed();
	}

	public Object putScriptOrSource(ProceedingJoinPoint joinPoint) throws Throwable {
		final Object[] arguments = joinPoint.getArgs();
		final Map<String, String> pathVariables = (Map<String, String>) arguments[0];
		final DeveloperDependentEntity object = (DeveloperDependentEntity) arguments[1];
		final Authenticated principal = (Authenticated) ((UsernamePasswordAuthenticationToken) arguments[2]).getPrincipal();
		if (!principal.getId().equals(Long.parseLong(pathVariables.get("id")))) return forbid();
		final DeveloperEntity developer = developerService.get(principal.getId());
		object.setDeveloper(developer);
		return joinPoint.proceed();
	}

	public Object postScriptOrSource(ProceedingJoinPoint joinPoint) throws Throwable {
		final Object[] arguments = joinPoint.getArgs();
		final DeveloperDependentEntity object = (DeveloperDependentEntity) arguments[1];
		final Authenticated principal = (Authenticated) ((UsernamePasswordAuthenticationToken) arguments[2]).getPrincipal();
		final DeveloperEntity developer = developerService.get(principal.getId());
		object.setDeveloper(developer);
		return joinPoint.proceed();
	}

	@Around("put() && developer()")
	public Object putDeveloper(ProceedingJoinPoint joinPoint) throws Throwable {
		final Object[] arguments = joinPoint.getArgs();
		final Map<String, String> pathVariables = (Map<String, String>) arguments[0];
		final DeveloperEntity model = (DeveloperEntity) arguments[1];
		final Authenticated principal = (Authenticated) ((UsernamePasswordAuthenticationToken) arguments[2]).getPrincipal();
		if (!principal.getId().equals(Long.parseLong(pathVariables.get("id")))) return forbid();
		model.setId(principal.getId());
		return joinPoint.proceed();
	}

	@Around("delete() && developer()")
	public Object deleteDeveloper(ProceedingJoinPoint joinPoint) throws Throwable {
		final Object[] arguments = joinPoint.getArgs();
		final Map<String, String> pathVariables = (Map<String, String>) arguments[0];
		final Authenticated principal = (Authenticated) ((UsernamePasswordAuthenticationToken) arguments[1]).getPrincipal();
		return principal.getId().equals(Long.parseLong(pathVariables.get("id"))) ? (ResponseEntity<DeveloperEntity>)joinPoint.proceed() : forbid();
	}

}
