package com.jnk.kb.entities;

import lombok.Data;

import javax.persistence.MappedSuperclass;

/**
 * Created by Raman_Susla1
 * kb
 * 11/2/2017
 */
@MappedSuperclass
public abstract class DeveloperDependentEntity {
	public abstract DeveloperEntity getDeveloper();
	public abstract void setDeveloper(DeveloperEntity entity);
}

