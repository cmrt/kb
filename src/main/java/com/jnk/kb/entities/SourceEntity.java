package com.jnk.kb.entities;

import com.fasterxml.jackson.annotation.*;
import com.jnk.kb.views.View;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Raman_Susla1
 * kb
 * 11/28/2017
 */
@Entity
@Data
@Table(name = "source")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class SourceEntity extends DeveloperDependentEntity{
	@Id
	@JsonUnwrapped
	@JsonView({View.Advanced.class, View.Simple.class})
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "source_seq_gen")
	@SequenceGenerator(name = "source_seq_gen", sequenceName = "source_id_seq")
	@Column(name = "id")
	private Long id;
	@JsonView({View.Advanced.class, View.Simple.class})
	@Column(name = "uid")
	@NotBlank
	private String uid;

	@JsonView({View.Advanced.class, View.Simple.class})
	@JsonProperty("type")
	@Column(name = "type")
	private String type;

	@JsonView({View.Advanced.class, View.Simple.class})
	@OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, fetch = FetchType.EAGER, mappedBy = "source")
	@JsonManagedReference
	private List<AttributeEntity> attributes;

	@JsonView({View.Advanced.class, View.Simple.class})
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumns({
		@JoinColumn(name = "developer_id", referencedColumnName = "id", updatable = false)
	})
	private DeveloperEntity developer;
}
