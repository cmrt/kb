package com.jnk.kb.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.views.View;
import lombok.Data;
import org.hibernate.validator.constraints.Email;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Raman_Susla1
 * kb
 * 10/31/2017
 */
@Entity
@Data
@Table(name = "developer")
public class DeveloperEntity {
	@Id
	@JsonUnwrapped
	@JsonView({View.Advanced.class, View.Simple.class})
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "developer_seq_gen")
	@SequenceGenerator(name = "developer_seq_gen", sequenceName = "developer_id_seq")
	@Column(name = "id")
	private Long id;

	@Column(name = "uid")
	@NotBlank	@JsonView({View.Advanced.class, View.Simple.class})
	private String uid;

	@NotNull
	@Column(name = "password")
	private String password;

	@JsonIgnore
	public String getPassword() {
		return password;
	}
	@JsonProperty("password")
	public void setPassword(String password) {
		this.password = password;
	}

	@JsonView({View.Developer.class})
	@JsonProperty("email")
	@NotNull
	@Column(name = "email", unique = true)
	@Email
	private String email;

	@JsonIgnore
	@Column(name = "enabled")
	private boolean enabled;

	public DeveloperEntity(){
		super();
	}

	public DeveloperEntity(Long id)
	{
		this.id = id;
	}
}
