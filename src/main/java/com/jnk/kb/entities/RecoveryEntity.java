package com.jnk.kb.entities;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.views.View;
import lombok.Data;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Raman_Susla1
 * kb
 * 10/31/2017
 */
@Entity
@Data
@Table(name = "recovery")
public class RecoveryEntity extends DeveloperDependentEntity{
	@Id
	@JsonUnwrapped
	@JsonView(View.Simple.class)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "recovery_seq_gen")
	@SequenceGenerator(name = "recovery_seq_gen", sequenceName = "recovery_id_seq")
	@Column(name = "id")
	private Long id;

	@Column(name = "uid")
	private String uid;

	@Column(name = "expirationDate")
	private Date expirationDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name = "developer_id", referencedColumnName = "id")
	})
	private DeveloperEntity developer;

}
