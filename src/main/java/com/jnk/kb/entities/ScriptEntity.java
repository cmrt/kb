package com.jnk.kb.entities;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.views.View;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.List;

/**
 * Created by Raman_Susla1
 * kb
 * 10/31/2017
 */
@Entity
@Data
@Table(name = "script")
public class ScriptEntity extends DeveloperDependentEntity{
	@Id
	@JsonUnwrapped
	@JsonView({View.Advanced.class, View.Simple.class})
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "script_seq_gen")
	@SequenceGenerator(name = "script_seq_gen", sequenceName = "script_id_seq")
	@Column(name = "id")
	private Long id;

	@JsonView({View.Advanced.class, View.Simple.class})
	@Column(name = "uid")
	@NotBlank
	private String uid;
	@JsonView({View.Advanced.class, View.Simple.class})

	@JsonProperty("description")
	@Column(name = "description")
	private String description;
	@JsonView({View.Advanced.class, View.Simple.class})
	@JsonProperty("code")
	@Column(name = "code")
	private String code;

	@JsonView({View.Advanced.class})
	@ManyToMany(cascade = CascadeType.ALL)
	@ElementCollection
	@JoinTable(name = "script_source",
		inverseJoinColumns = {@JoinColumn(name = "source_id", referencedColumnName = "id")},
		joinColumns = {@JoinColumn(name = "script_id", referencedColumnName = "id")})
	private List<SourceEntity> sources;

	@JsonView({View.Advanced.class, View.Simple.class})
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name = "developer_id", referencedColumnName = "id")
	})
	private DeveloperEntity developer;
}
