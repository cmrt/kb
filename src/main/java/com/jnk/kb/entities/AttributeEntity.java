package com.jnk.kb.entities;

import com.fasterxml.jackson.annotation.*;
import com.jnk.kb.views.View;
import lombok.Data;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

/**
 * Created by Raman_Susla1
 * kb
 * 11/28/2017
 */
@Entity
@Data
@Table(name = "attribute")
public class AttributeEntity {
	@Id
	@JsonUnwrapped
	@JsonView(View.Simple.class)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "attribute_seq_gen")
	@SequenceGenerator(name = "attribute_seq_gen", sequenceName = "attribute_id_seq")
	@Column(name = "id")
	private Long id;

	@Column(name = "uid")
	@NotNull
	private String uid;

	@JsonView(View.Simple.class)
	@JsonProperty("value")
	@Column(name = "value")
	private String value;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "source_id", referencedColumnName = "id")
	@JsonBackReference
	private SourceEntity source;
}
