package com.jnk.kb.entities;

import com.fasterxml.jackson.annotation.JsonUnwrapped;
import com.fasterxml.jackson.annotation.JsonView;
import com.jnk.kb.views.View;
import lombok.Data;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import java.util.Date;

/**
 * Created by Raman_Susla1
 * kb
 * 10/31/2017
 */
@Entity
@Data
@Table(name = "verification")
public class VerificationEntity extends DeveloperDependentEntity{
	@Id
	@JsonUnwrapped
	@JsonView(View.Simple.class)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "verification_seq_gen")
	@SequenceGenerator(name = "verification_seq_gen", sequenceName = "verification_id_seq")
	@Column(name = "id")
	private Long id;

	@Column(name = "uid")
	@NotBlank
	private String uid;

	@Column(name = "expirationDate") private Date expirationDate;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumns({
		@JoinColumn(name = "developer_id", referencedColumnName = "id")
	})
	private DeveloperEntity developer;
}
