package com.jnk.kb.models;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by catmo_000 on 12/2/2017.
 */
@Data
@AllArgsConstructor
public class Action {
	@JsonProperty("method") private String method;
	@JsonProperty("path") private String path;
}
