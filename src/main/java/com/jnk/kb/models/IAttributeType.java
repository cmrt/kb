package com.jnk.kb.models;

/**
 * Created by catmo_000 on 12/8/2017.
 */
public interface IAttributeType {
	String getName();
	boolean isMandatory();
}
