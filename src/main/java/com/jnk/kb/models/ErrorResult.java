package com.jnk.kb.models;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
@Data
@AllArgsConstructor
public class ErrorResult {
	@JsonProperty("message") private String message;
	@JsonProperty("actions") @JsonInclude(JsonInclude.Include.NON_NULL) private List<Action> actions;

	public ErrorResult(String message)
	{
		this.message = message;
	}
}
