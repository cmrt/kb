package com.jnk.kb.models;

import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by Raman_Susla1
 * kb
 * 11/24/2017
 */
@Data
@AllArgsConstructor
public class EvaluationResult {
	private Object data;
}