package com.jnk.kb.models.mails;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.RecoveryEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by catmo_000 on 12/5/2017.
 */
@Data
@AllArgsConstructor
public class RecoveryMail {
	private DeveloperEntity developer;
	private RecoveryEntity recovery;
}
