package com.jnk.kb.models.mails;

import com.jnk.kb.entities.DeveloperEntity;
import com.jnk.kb.entities.VerificationEntity;
import lombok.AllArgsConstructor;
import lombok.Data;

/**
 * Created by catmo_000 on 12/5/2017.
 */
@Data
@AllArgsConstructor
public class ConfirmationMail {
	private DeveloperEntity developer;
	private VerificationEntity verification;
}
